const { Users } = require("../models");

const format = (Users) => {
  const { id, name } = Users;

  return {
    id,
    name,
    token: Users.generateToken(),
  };
};

exports.register = async (req, res) => {
  try {
    await Users.register(req.body)
      .then((data) => {
        res.json({ status: "Register Success", data });
      })
      .catch((err) => {
        res.status(400).json({ status: "Register Failed", msg: err });
      });
  } catch (err) {
    res.status(500).json({ status: "Register Failed", msg: err });
  }
};

exports.login = (req, res) => {
  try {
    Users.auth(req.body)
      .then((data) => {
        res.json({ status: "Login Success", data: format(data) });
      })
      .catch((err) => {
        res.status(401).json({ status: "Login Failed", msg: err });
      });
  } catch (err) {
    res.status(500).json({ status: "Login Failed", msg: err.message });
  }
};
