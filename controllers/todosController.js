const { Users, todos } = require("../models");

exports.getTodos = async (req, res) => {
  try {
    const user = await Users.findByPk(req.params.userId);
    if (!user) {
      return res.status(404).json({ status: "User not found" });
    }

    await todos
      .findAll({
        where: { userId: user.id },
      })
      .then((data) => {
        res.status(200).json({ status: "Success", data });
      })
      .catch((err) => {
        res.status(500).json({ status: "Error", msg: err.message });
      });
  } catch (err) {
    res.status(500).json({ status: "Error", msg: err.message });
  }
};

exports.createTodo = async (req, res) => {
  try {
    const user = await Users.findByPk(req.params.userId);
    if (!user) {
      return res.status(404).json({ status: "User not found" });
    }

    const { todo } = req.body;

    if (!todo || todo.trim() === "") {
      return res
        .status(400)
        .json({ status: "Error", msg: "Please Fill the To Do" });
    }

    await todos
      .create({
        userId: user.id,
        todo,
      })
      .then((data) => {
        res.status(200).json({ status: "Success", data });
      })
      .catch((err) => {
        res.status(500).json({ status: "Error", msg: err.message });
      });
  } catch (err) {
    res.status(500).json({ status: "Error", msg: err.message });
  }
};

exports.updateTodo = async (req, res) => {
  try {
    const user = await Users.findByPk(req.params.userId);
    if (!user) {
      return res.status(404).json({ status: "User not found" });
    }

    const todo = await todos.findOne({
      where: { id: req.params.id, userId: user.id },
    });
    if (!todo) {
      return res.status(404).json({ status: "Todo not found" });
    }

    todo.completed = req.body.completed;
    await todo.save();

    res.json({ status: "Success", data: todo });
  } catch (err) {
    res.status(500).json({ status: "Error", msg: err.message });
  }
};

exports.deleteTodo = async (req, res) => {
  try {
    const user = await Users.findByPk(req.params.userId);
    if (!user) {
      return res.status(404).json({ status: "User not found" });
    }

    const todo = await todos.findOne({
      where: { id: req.params.id, userId: user.id },
    });
    if (!todo) {
      return res.status(404).json({ status: "Todo not found" });
    }

    await todo.destroy();

    res.json({ status: "Success" });
  } catch (err) {
    res.status(500).json({ status: "Error", msg: err.message });
  }
};
