const { Users } = require("../models");

exports.showAll = (req, res) => {
  try {
    Users.findAll()
      .then((data) => {
        res.json({ status: "Successfully Show All Data", data: data });
      })
      .catch((err) => {
        res.status(400).json({ status: "Failed Show Data", msg: err });
      });
  } catch (err) {
    res.status(500).json({ status: "Failed Show Data", msg: err });
  }
};
