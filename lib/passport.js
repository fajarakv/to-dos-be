const passport = require("passport");
const { Users } = require("../models");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

const options = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: "This is my secret",
};

passport.use(
  new JwtStrategy(options, async (payload, done) => {
    Users.findByPk(payload.id)
      .then((Users) => done(null, Users))
      .catch((err) => done(err, false));
  })
);

module.exports = passport;
