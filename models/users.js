"use strict";
const { Model } = require("sequelize");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    // static #encrypt = (pin) => bcrypt.hashSync(pin, 10);

    static register = async ({ name, pin }) => {
      // const encryptedPin = Users.#encrypt(pin);

      if (name === "") return Promise.reject("Fill name first");
      if (pin === "") return Promise.reject("Fill PIN first");

      const pinRegex = /^\d{4}$/;
      if (!pinRegex.test(pin)) {
        return Promise.reject("Please use 4-digit numeric only PIN");
      }

      const isUserExist = await Users.findOne({ where: { pin } });

      if (!isUserExist) {
        return Users.create({
          name,
          pin,
        });
      } else {
        return Promise.reject("PIN Existed, Choose Another PIN");
      }
    };

    // checkPin = (pin) => bcrypt.compareSync(pin, this.pin);

    generateToken = () => {
      const payload = {
        id: this.id,
        name: this.name,
      };

      const secret = "This is my secret";
      const token = jwt.sign(payload, secret);

      return token;
    };

    static auth = async ({ pin }) => {
      try {
        // const isPassValid = user.checkPin(pin);
        const user = await this.findOne({ where: { pin } });

        if (!user) {
          return Promise.reject("Wrong PIN, User Not Found");
        }

        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  Users.init(
    {
      name: DataTypes.STRING,
      pin: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Users",
    }
  );
  return Users;
};
