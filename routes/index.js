var express = require("express");
var router = express.Router();

const authRouter = require("./auth");
const usersRouter = require("./users");
const todosRouter = require("./todos");

router.use("/auth", authRouter);
router.use("/user", usersRouter);
router.use("/todos", todosRouter);

module.exports = router;
