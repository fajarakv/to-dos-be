const router = require("express").Router();
const todos = require("../controllers/todosController");

router.post("/:userId", todos.createTodo);
router.get("/:userId", todos.getTodos);
router.delete("/:userId/:id", todos.deleteTodo);
router.put("/:userId/:id", todos.updateTodo);

module.exports = router;
